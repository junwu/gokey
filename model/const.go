package model

import (
	"gitlab.com/junwu/w32"
)

var XmlSetting Setting

var HWnd w32.HWND
var PID uint32

var LevelIndex = 1
var (
	HMHook w32.HHOOK
	HKHook w32.HHOOK
)

const (
	VK_A_KEY uint16 = 0x41

	VK_S_KEY uint16 = 0x53
)

const (
	MK_MBUTTON = 0x0010
)
