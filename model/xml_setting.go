package model

type Item struct {
	Key      string  `xml:"key"`
	Interval float32 `xml:"interval"`
}

type Wheel struct {
	UpKey string `xml:"up_key"`
	DownKey string `xml:"down_key"`
}
type Batch struct {
	Key   string `xml:"key"`
	Items struct {
		Item []Item `xml:"item"`
	} `xml:"items"`
}


type Point struct {
	X int `xml:"x,attr"`
	Y int `xml:"y,attr"`
}
func (p *Point) IsNull() bool{
	if p.X == 0 || p.Y == 0 {
		return true
	}
	return false
}
type LevelPos struct {
	L1 Point `xml:"l_1"`
 	L2 Point `xml:"l_2"`
 	L3 Point `xml:"l_3"`
 	L4 Point `xml:"l_4"`
 	Reset Point `xml:"reset"`
 	Accept Point `xml:"accept"`
}

type AutoRightClick struct {
	Interval float32 `xml:"interval"`
	Enabled bool `xml:"enabled"`
}

type Setting struct {
	Diablo       string `xml:"diablo"`
	Diablo32       string `xml:"diablo32"`
	Wheel        Wheel   `xml:"wheel"`
	Clicked      Item   `xml:"clicked"`
	Num int `xml:"num"`
	LevelPos LevelPos `xml:"level_pos"`
	AutoRightClick AutoRightClick `xml:"auto_right_click"`
	Batch        Batch  `xml:"batch"`
}
