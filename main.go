package main

import (
	"flag"
	"log"
	"os"
	"./setting"
	"./model"
	"./win"
	"gitlab.com/junwu/w32"
	"time"
)


func main() {
	flag.Parse()

	println("---------------------------暗黑3自动按键小程序0.3版---------------------------")
	//update.CheckVersion()

	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	println("working directory:", dir)
	isOk := setting.LoadXml(&model.XmlSetting)


	win.InitChannel()
	//println("注意事项：")
	//println("1、如果需要在放批量技能的时候改变巅峰等级，请先打开巅峰等级面板，按下Ctrl键从上到下，依次右键点击按钮位置（共6个），然后松开Ctrl键")
	//println("2、窗口大小改变之后需要重新设置位置")
	//println("3、配置文件中的num 计算方式为：(第一页点数 - 50) / 100，有余数需要 + 1" )
	//println("3、批量技能是通鼠标中键切换" )
	//println("4、如果使用问题，请联系awfan.cn@gmail.com" )


	go func() {
		win.FindPid()
		if model.PID != 0 {
			win.FindTopWindow()
		}

		for {
			select {
			case <-time.After(3 * time.Second):
				win.FindPid()
				if model.PID != 0 {
					win.FindTopWindow()
				}
			}
		}

	}()

	hInst := w32.GetModuleHandle("")

	if isOk {
		model.HMHook = w32.SetWindowsHookEx(w32.WH_MOUSE_LL, win.MouseHook, hInst, 0)
		model.HKHook = w32.SetWindowsHookEx(w32.WH_KEYBOARD_LL, win.KeyboardHook, hInst, 0)
	}

	var msg w32.MSG
	for {
		if w32.GetMessage(&msg, 0, 0, 0) <= 0 {
			break
		}
		w32.TranslateMessage(&msg)
		w32.DispatchMessage(&msg)
	}

	if isOk {
		w32.UnhookWindowsHookEx(model.HMHook)
		w32.UnhookWindowsHookEx(model.HKHook)
	}
}
