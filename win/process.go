package win

import (
	"gitlab.com/junwu/w32"
	"unsafe"
	"../model"
	"strings"
	"syscall"
)

const TH32CS_SNAPPROCESS = 0x00000002

func FindPid()  {
	var pe32 w32.PROCESSENTRY32

	pe32.Size = uint32(unsafe.Sizeof(pe32))

	snapshot := w32.CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0)

	if snapshot == w32.ERROR_INVALID_HANDLE {
		model.PID = 0
		return
	}

	defer w32.CloseHandle(snapshot)

	if !w32.Process32First(snapshot, &pe32) {
		model.PID = 0
		return
	}

	for
	{
		if !w32.Process32Next(snapshot, &pe32) {
			model.PID = 0
			break
		}

		fileName := syscall.UTF16ToString(pe32.ExeFile[0:])

		if strings.Compare(fileName, model.XmlSetting.Diablo) == 0 {
			model.PID = pe32.ProcessID
			break
		} else if strings.Compare(fileName, model.XmlSetting.Diablo32) == 0 {
			model.PID = pe32.ProcessID
			break
		}
	}
}
var cb = syscall.NewCallback(func(h syscall.Handle, p uintptr) uintptr {
	_, processId := w32.GetWindowThreadProcessId(w32.HWND(h))
	model.HWnd = 0

	if processId == model.PID && w32.GetWindow(w32.HWND(h), w32.GW_OWNER) == 0 {
		model.HWnd = w32.HWND(h)
		return w32.FALSE // stop enumeration
	}


	return w32.TRUE // continue enumeration
})

func FindTopWindow(){
	// Enumerate the windows using a lambda to process each window
	w32.EnumWindows(cb, 0)
}

func isWorkingWindow() bool {
	h, _ := w32.GetForegroundWindow()

	return w32.HWND(h)  == model.HWnd
}
