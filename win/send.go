package win

import "gitlab.com/junwu/w32"

const KEYEVENTF_KEYUP = 0x0002 //key UP

func sendKey(vk uint16) {
	var inputs []w32.INPUT
	inputs = append(inputs, w32.INPUT{
		Type: w32.INPUT_KEYBOARD,
		Ki: w32.KEYBDINPUT{
			WVk:         vk,
			WScan:       0,
			DwFlags:     0, // no KEYEVENTF_KEYDOWN
			Time:        0,
			DwExtraInfo: 0,
		},
	})

	w32.SendInput(inputs)

	inputs = append(inputs, w32.INPUT{
		Type: w32.INPUT_KEYBOARD,
		Ki: w32.KEYBDINPUT{
			WVk:         vk,
			WScan:       0,
			DwFlags:     KEYEVENTF_KEYUP,
			Time:        0,
			DwExtraInfo: 0,
		},
	})

	w32.SendInput(inputs)
}

func moveMouse(x, y int) {
	var inputs []w32.INPUT

	inputs = append(inputs, w32.INPUT{
		Type: w32.INPUT_MOUSE,
		Mi: w32.MOUSEINPUT{
			MouseData: 0,
			Dx:      int32(x * (65536.0 / w32.GetSystemMetrics(w32.SM_CXSCREEN))),
			Dy:      int32(y * (65536.0 / w32.GetSystemMetrics(w32.SM_CYSCREEN))),
			DwFlags: w32.MOUSEEVENTF_ABSOLUTE | w32.MOUSEEVENTF_MOVE,
		},
	})

	w32.SendInput(inputs)
}

func sendClick(d, u uint32) {
	var inputs []w32.INPUT
	inputs = append(inputs, w32.INPUT{
		Type: w32.INPUT_MOUSE,
		Mi: w32.MOUSEINPUT{
			DwFlags: d,
		},
	})

	w32.SendInput(inputs)

	inputs = append(inputs, w32.INPUT{
		Type: w32.INPUT_MOUSE,
		Mi: w32.MOUSEINPUT{
			DwFlags: u,
		},
	})

	w32.SendInput(inputs)
}


func sendCtrlClick() {
	var inputs []w32.INPUT

	// ctrl
	inputs = append(inputs, w32.INPUT{
		Type: w32.INPUT_KEYBOARD,
		Ki: w32.KEYBDINPUT{
			WVk:         w32.VK_CONTROL,
			WScan:       0,
			DwFlags:     0, // no KEYEVENTF_KEYDOWN
			Time:        0,
			DwExtraInfo: 0,
		},
	})

	w32.SendInput(inputs)

	//inputs = inputs[:0]

	inputs = append(inputs, w32.INPUT{
		Type: w32.INPUT_MOUSE,
		Mi: w32.MOUSEINPUT{
			DwFlags: w32.MOUSEEVENTF_LEFTDOWN,
		},
	})

	w32.SendInput(inputs)

	inputs = append(inputs, w32.INPUT{
		Type: w32.INPUT_MOUSE,
		Mi: w32.MOUSEINPUT{
			DwFlags: w32.MOUSEEVENTF_LEFTUP,
		},
	})

	w32.SendInput(inputs)

	//inputs = inputs[:0]
	//ctrl
	inputs = append(inputs, w32.INPUT{
		Type: w32.INPUT_KEYBOARD,
		Ki: w32.KEYBDINPUT{
			WVk:         w32.VK_CONTROL,
			WScan:       0,
			DwFlags:     KEYEVENTF_KEYUP,
			Time:        0,
			DwExtraInfo: 0,
		},
	})

	w32.SendInput(inputs)
}
