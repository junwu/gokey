package win

import (
	"gitlab.com/junwu/w32"
	"unsafe"
	"strings"
)

func isTriggerKey(keyString string, lParam w32.LPARAM) bool {
	hKs := (*w32.KBDLLHOOKSTRUCT)(unsafe.Pointer(lParam))

	keys := strings.Split(keyString, "+")

	var isCtrlPressed = false
	var isAltPressed = false
	var isShiftPressed = false
	var vk uint16

	var isUseCtrl = false
	var isUseAlt = false
	var isUseShift = false
	var isUseVK = false

	for _, k := range keys {
		c := strings.ToUpper(strings.TrimSpace(k))
		switch c {
		case "CTRL":
			isUseCtrl = true
			isCtrlPressed = w32.GetAsyncKeyState(w32.VK_CONTROL)&uint16(0x8000) != 0
		case "ALT":
			isUseAlt = true
			isAltPressed = w32.GetAsyncKeyState(w32.VK_MENU)&uint16(0x8000) != 0
		case "SHIFT":
			isUseShift = true
			isShiftPressed = w32.GetAsyncKeyState(w32.VK_SHIFT)&uint16(0x8000) != 0
		default:
			if c == "" {
				break
			}
			isUseVK = true
			vk = uint16([]rune(strings.ToUpper(c))[0])
		}
	}

	if isUseCtrl && !isCtrlPressed {
		return false
	}
	if isUseShift && !isShiftPressed {
		return false
	}
	if isUseAlt && !isAltPressed {
		return false
	}
	if isUseVK && hKs.VkCode == w32.DWORD(vk) {
		return true
	}
	return false
}
