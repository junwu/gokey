package win

import (
	"strings"
	"time"
	"../model"
	"gitlab.com/junwu/w32"
)

func simKey(ch chan int, item model.Item) {
	c := strings.TrimSpace(item.Key)

	if c == "" {
		return
	}
	if isWorkingWindow() {
		sendKey(uint16([]rune(c)[0]))
	}

	for {
		select {
		case <-ch:
			return
		case <-time.After(time.Millisecond * time.Duration(item.Interval*1000)):
			{
				if isWorkingWindow() {
					sendKey(uint16([]rune(c)[0]))
				}
			}
		}
	}
}

func sendPKey() {
	c := strings.TrimSpace("P") // 大写

	if c == "" {
		return
	}
	if isWorkingWindow() {
		sendKey(uint16([]rune(c)[0]))
	}
}

func sendQKey() {
	c := strings.TrimSpace("Q") // 大写

	if c == "" {
		return
	}
	if isWorkingWindow() {
		sendKey(uint16([]rune(c)[0]))
	}
}


func simClick(ch chan int, interval float32) {
	if isWorkingWindow() {
		sendClick(w32.MOUSEEVENTF_LEFTDOWN, w32.MOUSEEVENTF_LEFTUP)
	}

	for {
		select {
		case <-ch:
			return
		case <-time.After(time.Millisecond * time.Duration(interval*1000)):
			{

				if isWorkingWindow() {
					sendClick(w32.MOUSEEVENTF_LEFTDOWN, w32.MOUSEEVENTF_LEFTUP)
				}
			}
		}
	}
}

func simRightClick(ch chan int, interval float32) {
	if isWorkingWindow() {
		sendClick(w32.MOUSEEVENTF_RIGHTDOWN, w32.MOUSEEVENTF_RIGHTUP)
	}

	for {
		select {
		case <-ch:
			return
		case <-time.After(time.Millisecond * time.Duration(interval*1000)):
			{
				if isWorkingWindow() {
					sendClick(w32.MOUSEEVENTF_RIGHTDOWN, w32.MOUSEEVENTF_RIGHTUP)
				}
			}
		}
	}
}
