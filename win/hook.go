package win

import (
	"../model"
	"../setting"
	"gitlab.com/junwu/w32"
	"unsafe"
	"log"
)

func MouseHook(nCode int, wParam w32.WPARAM, lParam w32.LPARAM) w32.LRESULT {
	if nCode == w32.HC_ACTION && (wParam == w32.WM_MBUTTONDOWN) {
		pMhs := (*w32.MSLLHOOKSTRUCT)(unsafe.Pointer(lParam))
		go triggerBatchKey(pMhs.Pt)
	} else if nCode == w32.HC_ACTION && (wParam == w32.WM_MOUSEWHEEL) {
		pMhs := (*w32.MSLLHOOKSTRUCT)(unsafe.Pointer(lParam))

		go triggerWheelKey(int16(w32.HIWORD(uint32(pMhs.MouseData))))
	} else if nCode == w32.HC_ACTION && (wParam == w32.WM_RBUTTONDOWN) {
		pMhs := (*w32.MSLLHOOKSTRUCT)(unsafe.Pointer(lParam))
		go triggerSetButtonPos(pMhs.Pt)
	}
	// Pass-through event.
	return w32.CallNextHookEx(model.HMHook, nCode, wParam, lParam)
}
func KeyboardHook(nCode int, wParam w32.WPARAM, lParam w32.LPARAM) w32.LRESULT {
	if nCode == w32.HC_ACTION && (wParam == w32.WM_KEYDOWN || wParam == w32.WM_SYSKEYDOWN) {
		if isTriggerKey(model.XmlSetting.Batch.Key, lParam) {
			x, y, _ := w32.GetCursorPos()
			go triggerBatchKey(w32.POINT{int32(x), int32(y)})
		} else if isTriggerKey(model.XmlSetting.Clicked.Key, lParam) {
			go triggerClick()
		}
	} else if nCode == w32.HC_ACTION && wParam == w32.WM_KEYUP {
		if model.LevelIndex != 1 {
			model.LevelIndex = 1
			log.Println("结束巅峰按钮操作位置设置")
			go 	setting.WriteXml(&model.XmlSetting)
		}
	}
	return w32.CallNextHookEx(model.HKHook, nCode, wParam, lParam)
}
