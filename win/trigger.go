package win

import (
	"../model"
	"strings"
	"gitlab.com/junwu/w32"
	"log"
)

var (
	channelsBatchKey []chan int
	isBatchKeyPaused = true
	isClickPaused    = true
	channelClick     chan int
	channelRightClick chan int
	channelWheel     chan int
)

func InitChannel() {
	channelWheel = make(chan int)
	channelClick = make(chan int)
	channelRightClick = make(chan int)

	for i := 0; i < len(model.XmlSetting.Batch.Items.Item); i++ {
		channelsBatchKey = append(channelsBatchKey, make(chan int))
	}
}
func triggerClick() {
	isClickPaused = !isClickPaused
	if isClickPaused {
		channelClick <- 0
	} else {
		go simClick(channelClick, model.XmlSetting.Clicked.Interval)
	}

}

func triggerBatchKey(pt w32.POINT) {
	isBatchKeyPaused = !isBatchKeyPaused

	if isBatchKeyPaused {
		for i, ch := range channelsBatchKey {
			ch <- i
		}
		channelRightClick <- 0 // 结束

		//if model.XmlSetting.Num == 0 {
		//	return
		//}
		//// 死灵恢复点数
		//sendPKey()
		//time.Sleep(10 * time.Millisecond)
		//moveToClickButton(model.XmlSetting.LevelPos.Reset) // reset
		//time.Sleep(50 * time.Millisecond)
		//moveToCtrlClickButton(model.XmlSetting.LevelPos.L3)
		//time.Sleep(20 * time.Millisecond)
		//for i := 0; i < model.XmlSetting.Num; i++ {
		//	time.Sleep(5 * time.Millisecond)
		//	moveToCtrlClickButton(model.XmlSetting.LevelPos.L2)
		//}
		//time.Sleep(20 * time.Millisecond)
		//moveToClickButton(model.XmlSetting.LevelPos.Accept) // accept
		//time.Sleep(10 * time.Millisecond)
		//moveMouse(int(pt.X), int(pt.Y))
		//time.Sleep(20 * time.Millisecond)
		//sendQKey()
	} else {
		//if model.XmlSetting.Num != 0 {
		//	sendPKey() // 改变点数
		//
		//	time.Sleep(10 * time.Millisecond)
		//	moveToClickButton(model.XmlSetting.LevelPos.Reset) // reset
		//	time.Sleep(50 * time.Millisecond)
		//	moveToCtrlClickButton(model.XmlSetting.LevelPos.L4)
		//	time.Sleep(20 * time.Millisecond)
		//	for i := 0; i < model.XmlSetting.Num; i++ {
		//		time.Sleep(5 * time.Millisecond)
		//		moveToCtrlClickButton(model.XmlSetting.LevelPos.L1)
		//	}
		//	time.Sleep(20 * time.Millisecond)
		//	moveToClickButton(model.XmlSetting.LevelPos.Accept) // accept
		//	time.Sleep(10 * time.Millisecond)
		//	moveMouse(int(pt.X), int(pt.Y))
		//}
		//
		for i, item := range model.XmlSetting.Batch.Items.Item {
			go simKey(channelsBatchKey[i], item)
		}

		if model.XmlSetting.AutoRightClick.Enabled {
			go simRightClick(channelRightClick, model.XmlSetting.AutoRightClick.Interval)
		}
	}
}

func triggerWheelKey(delta int16) {
	var c string
	if delta > 0 {
		c = strings.TrimSpace(model.XmlSetting.Wheel.UpKey)
	} else {
		c = strings.TrimSpace(model.XmlSetting.Wheel.DownKey)
	}
	if c == "" {
		return
	}
	if isWorkingWindow() {
		sendKey(uint16([]rune(c)[0]))
	}
}

func moveToClickButton(p model.Point) {

	xInt, yInt := w32.ClientToScreen(model.HWnd, p.X, p.Y)

	moveMouse(xInt, yInt)

	sendClick(w32.MOUSEEVENTF_LEFTDOWN, w32.MOUSEEVENTF_LEFTUP)
}

func moveToCtrlClickButton(p model.Point) {
	xInt, yInt := w32.ClientToScreen(model.HWnd, p.X, p.Y)

	moveMouse(xInt, yInt)

	sendCtrlClick()
}

func triggerSetButtonPos(pt w32.POINT) {
	if !isWorkingWindow() {
		return
	}
	isCtrlPressed := w32.GetAsyncKeyState(w32.VK_CONTROL)&uint16(0x8000) != 0
	if !isCtrlPressed {
		return
	}

	x, y, ok := w32.ScreenToClient(model.HWnd, int(pt.X), int(pt.Y))
	if !ok {
		log.Println("坐标转换失败,请联系开发人员!")
		return
	}
	switch model.LevelIndex {
	case 1:
		model.XmlSetting.LevelPos.L1 = model.Point{x, y}
		log.Printf("设置巅峰+1位置：{%v, %v}", model.XmlSetting.LevelPos.L1.X, model.XmlSetting.LevelPos.L1.Y)
	case 2:
		model.XmlSetting.LevelPos.L2 = model.Point{x, y}
		log.Printf("设置巅峰+2位置：{%v,%v}", model.XmlSetting.LevelPos.L2.X,model.XmlSetting.LevelPos.L2.Y)
	case 3:
		model.XmlSetting.LevelPos.L3 = model.Point{x, y}
		log.Printf("设置巅峰+3位置：{%v,%v}", model.XmlSetting.LevelPos.L3.X,model.XmlSetting.LevelPos.L3.Y)
	case 4:
		model.XmlSetting.LevelPos.L4 = model.Point{x, y}
		log.Printf("设置巅巅峰+4位置：{%v,%v}", model.XmlSetting.LevelPos.L4.X,model.XmlSetting.LevelPos.L4.Y)
	case 5:
		model.XmlSetting.LevelPos.Reset = model.Point{x, y}
		log.Printf("设置巅峰重置位置：{%v,%v}", model.XmlSetting.LevelPos.Reset.X, model.XmlSetting.LevelPos.Reset.Y)
	case 6:
		model.XmlSetting.LevelPos.Accept = model.Point{x, y}
		log.Printf("设置巅峰接受位置：{%v,%v}", model.XmlSetting.LevelPos.Accept.X, model.XmlSetting.LevelPos.Accept.Y)
		return
	}
	model.LevelIndex = model.LevelIndex +1
}