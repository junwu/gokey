package update

import (
	"net/http"
	"io/ioutil"
	"fmt"
	"encoding/json"
)

//http://ozhlac1g2.bkt.clouddn.com/version.txt
const URL = "http://ozhlac1g2.bkt.clouddn.com/version.txt"
const VER_CODE = 2

type Version struct {
	Version     string            `json:"version"`
	VersionCode int32               `json:"version_code"`
	DownloadUrl string            `json:"download_url"`
	Description []string `json:"description"`
}

func CheckVersion() {
	resp, err := http.Get(URL)
	if err != nil {
		// handle error
		fmt.Println(err.Error())
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// handle error
		fmt.Println(err.Error())
		return
	}

	v := Version{}
	err = json.Unmarshal(body, &v)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	if v.VersionCode > VER_CODE {
		fmt.Printf("发现新版本:%v, 下载链接：%v\n", v.Version, v.DownloadUrl)
		fmt.Printf("新版本变化\n")

		for i, d := range v.Description {
			fmt.Printf("\t%v、%v\n", i + 1, d)
		}
	}
}
