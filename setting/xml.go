package setting

import (
	"io/ioutil"
	"encoding/xml"
	"../model"
	"fmt"
	"log"
	"flag"
	"os"
)

var xmlFile = flag.String("xml", "setting.xml", "配置文件")


func LoadXml(result *model.Setting) bool {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	file:= dir + "\\" + *xmlFile
	content, err := ioutil.ReadFile(file)

	if err != nil {
		println("配置文件解析错误", err.Error())
		return false
	}

	err = xml.Unmarshal(content, result)

	if err != nil {
		println("配置文件解析错误", err.Error())
		return false
	}
	return true
}

func WriteXml(result *model.Setting) {
	dir, _ := os.Getwd()
	file:= dir + "\\" + *xmlFile

	output, err := xml.MarshalIndent(result, "  ", "    ")
	if err != nil {
		fmt.Printf("error: %v\n", err)
	}

	err = ioutil.WriteFile(file, output, 0777) // 覆盖

	if err != nil {
		log.Println("write xml error:", err.Error())
	}
	//log.Println(string(output))
}